package com.lsktech.scalableservice.beans;

import com.lsktech.scalableservice.model.MovieEntity;

/**
 * 
 * @author svnrao
 *
 */
public class Movie {
	
	private String name;
	private String director;
	private int year;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public Movie build(MovieEntity entity) {
		
		if (entity != null) {

			this.name = entity.getName();
			this.director = entity.getDirector();
			this.year = entity.getYear();
		}
		return this;
	}

}
