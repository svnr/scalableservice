package com.lsktech.scalableservice.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lsktech.scalableservice.beans.Movie;
import com.lsktech.scalableservice.model.MovieEntity;
import com.lsktech.scalableservice.repository.MovieRepository;

/**
 * 
 * @author svnrao
 *
 */
@Component
public class MovieService {

	private MovieRepository movieRepository;

	@Autowired
	public MovieService(MovieRepository mRepository) {
		this.movieRepository = mRepository;

	}

	public Movie save(Movie movie) {

		MovieEntity entity = new MovieEntity();
		entity.buildEntity(movie);
		MovieEntity savedEntity = null;

		try {
			savedEntity = this.movieRepository.save(entity);
		} catch (Exception e) {
			throw new RuntimeException(e);// Throw custom exception.
		}

		return new Movie().build(savedEntity);

	}

	public List<Movie> getAllMovies() {

		Iterable<MovieEntity> iter = this.movieRepository.findAll();

		List<MovieEntity> result = StreamSupport.stream(iter.spliterator(), false).collect(Collectors.toList());

		return result.stream().map(me -> new Movie().build(me)).collect(Collectors.toList());

	}

}
