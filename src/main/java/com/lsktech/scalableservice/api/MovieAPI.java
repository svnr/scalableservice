package com.lsktech.scalableservice.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.lsktech.scalableservice.beans.Movie;
import com.lsktech.scalableservice.service.MovieService;

@Controller
public class MovieAPI {
	
	private List<Movie> moveis = new ArrayList<Movie>();
	
	@Autowired
	private MovieService service;
	
	@GetMapping(path = "/movies")
	public ResponseEntity<List<Movie>> getAllMovies(){
		List<Movie> result = this.service.getAllMovies();
		
		
		return new ResponseEntity<>(result,HttpStatus.OK);
		
	}
	
	@PostMapping(path = "/movie")
	public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
		
		Movie result;
		try {
			result = service.save(movie);
		} catch (Exception e) {
			throw e;
		}
		
		return new ResponseEntity<Movie>(result,HttpStatus.CREATED);
	}
	
	
	
	

}
