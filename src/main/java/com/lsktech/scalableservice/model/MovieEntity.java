package com.lsktech.scalableservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lsktech.scalableservice.beans.Movie;

/**
 * 
 * @author svnrao
 *
 */
@Entity
@Table(name = "movie")
public class MovieEntity {
	
	@Id
	@Column(name = "name")
	private String name;
	
	@Column(name = "director")
	private String director;
	
	@Column(name = "year")
	private Integer year;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public MovieEntity buildEntity(Movie movie) {
		if (movie != null) {
			this.name = movie.getName();
			this.director = movie.getDirector();
			this.year = movie.getYear();
		}
		return this;
	}

}
