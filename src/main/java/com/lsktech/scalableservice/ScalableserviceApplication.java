package com.lsktech.scalableservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScalableserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScalableserviceApplication.class, args);
	}

}
