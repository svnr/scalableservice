package com.lsktech.scalableservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.lsktech.scalableservice.model.MovieEntity;

public interface MovieRepository extends CrudRepository<MovieEntity, String>{

}
