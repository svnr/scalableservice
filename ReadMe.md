
* Pull Docker:

	https://hub.docker.com/_/postgres
	docker pull postgres:[tag_you_want]
	May be 9.5

* Create a Directory to Serve as the Local Host Mount Point for Postgres Data Files

	mkdir -p $HOME/docker/volumes/postgres
	
* Run Docker postgresql

	docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres:<version>

* Check docker container is running :

	Docker container list (This command lists the docker containers running in your host/laptop. Note the 	container named " pg-docker" is seen)
	
* Go inside your container and create a database:
	
	docker exec -it <image name or id>  bash
	psql -U postgres
	Run commands you want or create a data base
	\q to quit
	
	
* Connect to Postgres
	
	psql -h localhost -U postgres -d postgres
	

# Install pgsql client libraries on host machine.

	brew install libpq
	echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> ~/.bash_profile


# Create table movie in lsk schema

	CREATE TABLE lsk.movie(name varchar(50) UNIQUE NOT NULL, 	director VARCHAR (30)  NOT NULL, year integer);

	psql -h localhost -U postgres -d lsk
	
	select * from pg_catalog.pg_tables where schemaname='lsk'
	
## JMeter
	
	brew install jmeter
	
	Note : Plugins folder = CELLAR_HOME/jmeter/{version}/libexec/lib/ext
	usually CELLAR_HOME  = /usr/local/Cellar

	
